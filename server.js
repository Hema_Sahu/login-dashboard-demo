const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const flash=require("connect-flash");
const cookieSession = require('cookie-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
var mongojs=require('mongojs');
const MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var session = require("express-session");

var db=mongojs('litmus-users'); 
var url = 'mongodb://localhost:27017';

const app = express();

// BodyParser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(__dirname+'/public'));
app.use(cookieParser());
app.use(flash());

//cookie-session
// app.use(
//     cookieSession({
//       maxAge: 30 * 24 * 60 * 60 * 1000,
//       keys: 'sadfjksdhafjhsajkdfhjksdhajfkhsdjasf3erwqsa'
//     })
// );

app.use(session({ secret: "cats", resave: false, saveUninitialized: true}));

// Passport init
app.use(passport.initialize());
app.use(passport.session());


passport.use(new LocalStrategy(
     function(username, password, done) {
        console.log(username, password)
     	var url = 'mongodb://localhost:27017/litmus-users';
         MongoClient.connect(url, (err, db) => {
             if(err) {
                 throw err;
             } 
             db.collection('managers').find({ username: username}).toArray().then((user) => {
                 console.log(user);
                 if (err) { return done(err); }
                 if (user.length < 0) {
                 	console.log('user not find');
                     return done(null, false, { message: 'Incorrect username.' });
                 }
                 if (user[0].password !== password) {
                 	console.log('password not matched');
                     return done(null, false, { message: 'Incorrect password.' });
                 }

                 console.log('Success'+JSON.stringify(user[0]));
                return done(null, user[0]);
            }, (err) => {
                 throw err;
             });
             db.close();
         });
     })
 );


passport.serializeUser(function(user, done) {
    console.log('Serialize user:'+JSON.stringify(user));
    done(null, user);
});
  
passport.deserializeUser(function(user, done) {
    console.log("Deserialize called...",user);
    done(null, user);
});


app.post('/login',passport.authenticate('local', { 
                            successRedirect: '/success',
                           failureRedirect: '/failure',
                           failureFlash: true })
);


app.get('/success', (req, res) => {
    res.sendFile(__dirname+'/public/show_feeds.html');
    
});

app.get('/failure', (req, res) => {
    res.status(401).send('Invalid');
});


app.get('/current-user', (req, res) => {
    res.status(200).send(JSON.stringify(req.user));
        
});

app.get('/show-feeds',(req,res)=> {
     if(req.user.access.has_access === "false"){
        res.send('Access Denied');
    }
    var brand_id = new ObjectID(req.user.access.brand_id)
    db.collection('feeds').find({brand_id: brand_id},function(err,doc){
            res.json(doc);
    }); 
  
});


app.get('/userlist',function(req,res){
	console.log('I received a get request');

	db.collection('users').find(function(err,docs){
		console.log(docs);
		res.json(docs);
	})
});

app.post('/userlist',function(req,res){
	console.log(req.body);
	db.collection('users').insert(req.body,function(err,doc){
		res.json(doc);
	})
});

app.delete('/userlist/:id',function(req,res){
	var id=req.params.id;
	console.log(id);
	db.collection('users').remove({_id: mongojs.ObjectId(id)},function(err,doc){
		res.json(doc);
	});
});

app.get('/userlist/:id',function(req,res){
	var id=req.params.id;
	console.log(id);
	db.collection('users').findOne({_id: mongojs.ObjectId(id)},function(err,doc){
		res.json(doc);
	});
});

app.put('/userlist/:id',function(req,res){
	var id=req.params.id;
	console.log(req.body.name);
	db.collection('users').findAndModify({query: {_id: mongojs.ObjectId(id)},
	update: {$set: {name: req.body.name, email: req.body.email, number: req.body.number}},
	new: true},function(err,doc){
		res.json(doc);
	})
	
});


app.listen(3000);
console.log('server running on port 3000');

